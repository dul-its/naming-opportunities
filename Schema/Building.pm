package duaanaming::Schema::Result::Building;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('buildings');
__PACKAGE__->add_columns(qw/ id name label sublabel active weight featured created_at updated_at /);
__PACKAGE__->has_primary_key('id');
__PACKAGE__->has_many(floorplans => 'duaanaming::Schema::Result::Floorplan', 'building_id');

1;
