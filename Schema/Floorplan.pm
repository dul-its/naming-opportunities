package duaanaming::Schema::Result::Floormap;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('floorplans');
__PACKAGE__->add_columns(qw/ id name label sublabel building_id created_at updated_at floorplan_map_id /);
__PACKAGE__->has_primary_key('id');
__PACKAGE__->belongs_to(building => 'duaaname::Schema::Result::Building', 'id');

1;
