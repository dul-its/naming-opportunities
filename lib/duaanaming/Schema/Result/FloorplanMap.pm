use utf8;
package duaanaming::Schema::Result::FloorplanMap;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::FloorplanMap

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<floorplan_maps>

=cut

__PACKAGE__->table("floorplan_maps");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 label

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 image_url

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 img_width

  data_type: 'smallint'
  default_value: 0
  is_nullable: 1

=head2 img_height

  data_type: 'smallint'
  default_value: 0
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "label",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "image_url",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "img_width",
  { data_type => "smallint", default_value => 0, is_nullable => 1 },
  "img_height",
  { data_type => "smallint", default_value => 0, is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 floorplans

Type: has_many

Related object: L<duaanaming::Schema::Result::Floorplan>

=cut

__PACKAGE__->has_many(
  "floorplans",
  "duaanaming::Schema::Result::Floorplan",
  { "foreign.floorplan_map_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-01-31 17:14:18
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yjF04vm0gPDWx/nYT6s9mg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
