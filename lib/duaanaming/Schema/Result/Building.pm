use utf8;
package duaanaming::Schema::Result::Building;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::Building

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<buildings>

=cut

__PACKAGE__->table("buildings");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 label

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 sublabel

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 active

  data_type: 'tinyint'
  is_nullable: 1

=head2 weight

  data_type: 'smallint'
  default_value: 0
  is_nullable: 1

=head2 featured

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "label",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "sublabel",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "active",
  { data_type => "tinyint", is_nullable => 1 },
  "weight",
  { data_type => "smallint", default_value => 0, is_nullable => 1 },
  "featured",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 building_images

Type: has_many

Related object: L<duaanaming::Schema::Result::BuildingImage>

=cut

__PACKAGE__->has_many(
  "building_images",
  "duaanaming::Schema::Result::BuildingImage",
  { "foreign.building_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 floorplans

Type: has_many

Related object: L<duaanaming::Schema::Result::Floorplan>

=cut

__PACKAGE__->has_many(
  "floorplans",
  "duaanaming::Schema::Result::Floorplan",
  { "foreign.building_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-02-06 16:43:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iWmUjLrqBpjgGgB0CTTPQw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
