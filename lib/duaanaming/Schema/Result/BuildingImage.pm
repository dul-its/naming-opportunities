use utf8;
package duaanaming::Schema::Result::BuildingImage;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

duaanaming::Schema::Result::BuildingImage

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<building_image>

=cut

__PACKAGE__->table("building_image");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 image_url

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 building_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 created_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 updated_at

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "image_url",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "building_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 1 },
  "created_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "updated_at",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 building

Type: belongs_to

Related object: L<duaanaming::Schema::Result::Building>

=cut

__PACKAGE__->belongs_to(
  "building",
  "duaanaming::Schema::Result::Building",
  { id => "building_id" },
  {
    is_deferrable => 1,
    join_type     => "LEFT",
    on_delete     => "RESTRICT",
    on_update     => "RESTRICT",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-02-06 16:43:34
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:qavi66p1yy9xtp+UNPUYMg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
