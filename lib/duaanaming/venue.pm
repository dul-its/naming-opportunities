package duaanaming::venue;

use strict;
use warnings;
use Carp;
use Data::Dumper;
use Number::Format qw(:subs);

use Dancer2 appname => 'duaanaming';
use Dancer2::Plugin::DBIC;
use Dancer2::Plugin::Ajax;
use duaanaming::Helpers::ImageUtils qw(generated_floorplan_image);

hook before => sub {
	my $rs = schema->resultset('Building')->search(undef, {
		group_by => [qw/ name /],
		join => 'floorplans',
		order_by => [ { -desc => 'me.weight' }, 'me.label']
	});
	my @venues = $rs->all;

	# load the 'default floorplan' (floorplans where is_default = 1)
	# or get the first floorplan (based on alphabet sort order)
	grep {
		my $default_floorplan_rs = $_->search_related('floorplans', { is_default => 1 });
		my $default_floorplan = $default_floorplan_rs->first();
		if ( $default_floorplan ) {
			$_->{first_floorplan} = $default_floorplan;
		} else {
			my $sorted_floorplans = $_->search_related('floorplans', undef, { order_by => [ { -asc => 'weight' }, 'label' ] });
			$_->{first_floorplan} = $sorted_floorplans->first();
		}
	} @venues;

	var all_venues => \@venues;
};

hook before_template_render => sub {
	my $tokens = shift;
	$tokens->{all_venues} = vars->{all_venues};
};

prefix '/level';

get '/:level' => sub {
	my $level = route_parameters->get('level');

	my $floorplan_rs = schema->resultset('Floorplan');
	my $floorplan = $floorplan_rs->find({ 'me.name' => $level });

	my @sorted_floorplans = $floorplan->building->floorplans->search(undef, { order_by => { -asc => 'weight' } } );
	foreach my $s (@sorted_floorplans) {
		debug Dumper ($s->label);
	}

	# rediect to a 404 if not found.
	if ( !defined $floorplan ) {
		status 404;
		return template '404.tt', { error_message => 'Unable to locate this floorplan or level.' };
	}
	
	my @rooms = $floorplan->rooms->search(undef, { order_by => { -asc => 'me.id' } } );
	my @areas = ();

	# prepare display data for each room object
	foreach (@rooms) {
		my $label = $_->label;
		$label =~ s/\\n/<br \/>/g;
		$_->{filtered_label} = $label;

		my $naming_amount_formatted = format_price( $_->naming_amount, 0, '$' );
		my $gift = $naming_amount_formatted;
		$gift .= ' ' . $_->naming_amount_desc unless !$_->naming_amount_desc;

		$_->{gift} = $gift;
		$_->{naming_amount_formatted} = format_price( $_->naming_amount, 0, '$' );
		$_->{display_title_or_label} = length($_->display_title) ? $_->display_title : $_->label;

		foreach my $a ($_->room_areas) {
			my $o = { coord => $a->coord, shape => $a->shape };
			$o->{nameable} = $_->nameable;
			$o->{label_and_amount} = $_->label;
			if ($_->naming_amount) {
				$o->{label_and_amount} .= ' (' . $_->naming_amount;
				if ( $_->other_amount ) {
					$o->{label_and_amount} .= ' & ' . $_->other_amount . ' ' . $_->other_amount_desc;
				}
				$o->{label_and_amount} .= ')';
			}
			push @areas, $o;
		}
	}
	my $colors = {
		nameable => setting('rooms')->{colors}->{nameable},
		pending => setting('rooms')->{colors}->{pending},
		notnameable => setting('rooms')->{colors}->{notnameable},
	};
	if (config->{rooms}->{$floorplan->name}) {
		$colors->{nameable} = config->{rooms}->{$floorplan->name}->{colors}->{nameable};
		$colors->{notnameable} = config->{rooms}->{$floorplan->name}->{colors}->{notnameable};
	}

	@areas = reverse @areas;

	my $map = $floorplan->floorplan_map;
	my $r = generated_floorplan_image( 
		path_original		=> setting('image_base') . $map->image_url,
		image_name			=> $map->label,
		generated_base	=> setting('generated_image_base'),
		width 					=> $map->img_width, 
		height 					=> $map->img_height,
		areas						=> \@areas,
		colors					=> $colors,
	);
	$floorplan->floorplan_map->{active_image_path} = $r;

	my $rooms_rs = $floorplan->related_resultset('rooms');
	my @all_rooms = $rooms_rs->all();

	my @named_spaces = $rooms_rs->search( 
		{ nameable => 0 },
		{ order_by => [ { -asc => 'display_weight' }, 'me.label' ], group_by => 'me.label' }
	);
	grep {
		my $label = $_->label;
		$label =~ s/\\n/<br \/>/g;
		$_->{filtered_label} = $label;
	} @named_spaces;

	my @available_spaces = $rooms_rs->search( 
		{ nameable => 1 },
		{ order_by => [ { -asc => 'naming_amount' }, { -asc => 'display_weight' }, 'me.label' ] }
	);

	grep {
		$_->{amount_label} = '';
		if ( $_->naming_amount ) {
			$_->{amount_label} .= format_price($_->naming_amount, 0, '$') . ' Gift';
			if ( length $_->naming_amount_desc ) {
				$_->{amount_label} .= ' ' . $_->naming_amount_desc;
			}
			if ( $_->other_amount ) {
				$_->{amount_label} .= ' & ' . format_price($_->other_amount, 0, '$') . ' ' . $_->other_amount_desc;
			}
		}
		$_->{amount_label} =~ s/\\n/<br \/>/g;
	} @available_spaces;
	
	push @{vars->{breadcrumbs}}, { title => $floorplan->building->label, url => '/v/' . $floorplan->building->name };
	push @{vars->{body_classes}}, 'venue';
	push @{vars->{extrajs}}, 
		{ src => request->uri_base . '/javascripts/jquery.imagemapster.min.js' },
		{ src => request->uri_base . '/javascripts/imagesloaded.pkgd.min.js' };

	# alert 'templating' to include this file
	# push @{vars->{include_js_template}}, 'venue_popover.js.tt';

	template 'venue', {
		venue => $floorplan->building,
		fp => $floorplan,
		all_rooms => \@all_rooms,
		sorted_floorplans => \@sorted_floorplans,
		named_spaces => \@named_spaces,
		available_spaces => \@available_spaces,
		spaces => \@rooms,
	};
};

prefix '/space';

# load room (space) data for AJAX call
ajax '/:space' => sub {
	my $space = route_parameters->get('space');

	my $room_rs = schema->resultset('Room');
	#my $room = $room_rs->find( { name => $space } );
	my $room = $room_rs->find( { id => $space } );
	$room->{display_title_or_label} = length($room->display_title) ? $room->display_title : $room->label;

	my $naming_amount_text = '';
	if ($room->nameable) {
		$naming_amount_text = sprintf( "This space is available for a gift of \$%s", format_number( $room->naming_amount ) );
		debug "naming_amount_text = [" . $naming_amount_text . "]";
		if ($room->naming_amount_desc) {
			my $n = $room->naming_amount_desc;
			$n =~ s/(\\n|\(|\))//g;
			$naming_amount_text .= ".<br />(" . $n . ")";
		} else {
			$naming_amount_text .= '.';
		}
	}

	my @room_mockups = $room->related_resultset('room_mockups')->search(undef, { order_by => [ { -asc => 'display_weight' }, 'id'] });
	#my @room_mockups = $room->room_mockups;
	my @images = ();
	my $mockups_base_path = setting('ui')->{spaces}->{images}->{room_mockup_base_path};

	map {
		my $a = {};
		$a->{image_url} = request->uri_base . '/images' . $mockups_base_path . '/' . $_->image;
		push @images, $a;
	} @room_mockups;

	return to_json { 
		mockups_base_path => setting('ui')->{spaces}->{images}->{room_mockup_base_path},
		room_mockups			=> \@images,
		space_desc 				=> $naming_amount_text,
		space_level 			=> $room->{display_title_or_label},
	};
};

1;
