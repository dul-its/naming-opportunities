package Dancer2::Plugin::DUAAWeb::Common;

use strict;
use warnings;
use Carp;
use Moo;
use Dancer2::Plugin;
use Data::Dumper;

has dancer2_plugin_database => (
	is				=> 'ro',
	lazy			=> 1,
	default		=>
		sub { $_[0]->app->with_plugin('Dancer2::Plugin::Database') },
	handles 	=> { plugin_database => 'database' },
	init_arg	=> undef,
);

has database => (
	is			=> 'ro',
	lazy		=> 1,
	default	=> sub {
		my $self = shift;
		$self->plugin_database( $self->db_connection_name );
	},
);

has db_connection_name => (
	is	=> 'ro',
);

1;
