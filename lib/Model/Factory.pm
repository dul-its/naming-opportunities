package duaanaming::Model;

use Module::Runtime 'use_module';
use Moo;

has db => is => ro => required => 1;

sub get {
	my ( $self, $entity_name ) = @_;
	use_module( __PACKAGE__ . "::$entity_name" )->new( db => $self->db );
}

true;
